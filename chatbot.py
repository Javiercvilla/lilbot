import aiml
import sys

aimlFiles = ["aparienciaFisica.aiml","estadoAnimo.aiml","insultos.aiml","saludos.aiml","astrologia.aiml","familia.aiml","nacionalidades.aiml","sinRespuestas.aiml","atributosPsicologicos.aiml","nombres.aiml","conectores.aiml","genero.aiml","profesiones.aiml","edad.aiml","infoUsuario.aiml",  "respuestaGenerales.aiml","valve.aiml"]

class chatbot:
	def __init__(self):
		self.k = aiml.Kernel()
		for f in aimlFiles:
			self.k.learn(f)
		self.k.setBotPredicate("name","Cristal")

	def respond(self, peer, msg):
		return self.k.respond(msg,peer)

	def storeAllSessions(self):
		pass

	def loadAllSessions(self):
		pass

if (__name__ == "__main__"):
	bot = chatbot()
	line = ""
	for line in iter(sys.stdin.readline,''):
		print bot.respond("Bob",line)
